
;;; transparency.el - Manage Emacs frames transparency with custom variables -*- lexical-binding: t; -*-

;; Copyright (c) 2021, Dmitry Bashkirov
;;
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without modification,
;; are permitted provided that the following conditions are met:
;;
;;     * Redistributions of source code must retain the above copyright notice,
;;       this list of conditions and the following disclaimer.
;;     * Redistributions in binary form must reproduce the above copyright notice,
;;       this list of conditions and the following disclaimer in the documentation
;;       and/or other materials provided with the distribution.
;;     * Neither the name of Dmitry Bashkirov nor the names of its contributors
;;       may be used to endorse or promote products derived from this software
;;       without specific prior written permission.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
;; A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
;; CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
;; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
;; PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
;; LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
;; NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
;; SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

;; Author: D. Bashkirov <dmbash@outlook.com>
;; Version: 1.0
;; Keywords: interface
;; URL: https://gitlab.com/dmbash/transparency.el

;;; Commentary:

;;; Code:



(defgroup transparency nil
  "Transparency group"
  )

(setq transparency-default-active-value 90)
(setq transparency-default-inactive-value 90)

(defun transparency-set (active-value inactive-value)
  "Sets VALUE to transparency of Emacs frames"
  (set-frame-parameter (selected-frame) 'alpha (list active-value inactive-value)))

(defun transparency-update ()
  "Updates transparency active and inactive values"
  (if transparency-switch
      (when (and (boundp 'transparency-active-value) (boundp' transparency-inactive-value))
	(transparency-set transparency-active-value transparency-inactive-value)
	nil)
    (transparency-set 100 100)))

(defcustom transparency-switch nil
  "Toggle of transparency"
  :type 'boolean
  :group 'transparency
  :set
  #'(lambda (var value)
      (customize-set-value var value)
      (transparency-update)))

(defcustom transparency-active-value transparency-default-active-value
  "Transparency active value"
  :type 'integer
  :group 'transparency
  :set
  #'(lambda (var value)
      (customize-set-value var value)
      (transparency-update)))

(defcustom transparency-inactive-value transparency-default-inactive-value
  "Transparency inactive value"
  :type 'integer
  :group 'transparency
  :set
  #'(lambda (var value)
      (customize-set-value var value)
      (transparency-update)))

(transparency-update)

(provide 'transparency)
;;; transparency.el ends here
